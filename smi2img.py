#!/usr/bin/env python3

import argparse
import os
from rdkit import Chem
from rdkit.Chem import Draw


def convert(smiles_filename, png_path=os.getcwd(), output_format='png', scale=1.0, color=False):
    Draw.DrawingOptions.atomLabelFontSize = 16

    basename = os.path.basename(smiles_filename)
    root, ext = os.path.splitext(basename)

    with open(smiles_filename, 'r') as stream:
        for lineno, line in enumerate(stream):
            smiles = line.split()[0]
            image_filename = '{0}.{1}.{2}'.format(root, lineno + 1, output_format)
            image_filename = os.path.join(png_path, image_filename) 

            molecule = Chem.MolFromSmiles(smiles)
            Draw.MolToFile(molecule, image_filename, (800, 800))


parser = argparse.ArgumentParser(description='Draw chemical compounds in SMI file')
parser.add_argument('--input', metavar='smi-filename', type=str, 
                    dest='smiles_filename', required=True, 
                    help='designate an input SMI file (required)')
parser.add_argument('--output', metavar='output-path', type=str, 
                    dest='output_path', default=os.getcwd(), 
                    help='designate an output directory (default: current directory)')
"""
parser.add_argument('--color', metavar='[true|false]', type=str, 
                    choices=['true', 'false'], dest='color', default='false', 
                    help='designate whether to use color (default: false)')
parser.add_argument('--format', metavar='[png|svg|pdf]', type=str, 
                    choices=['png', 'svg', 'pdf'], dest='format', default='png', 
                    help='designate an output format (default: png)')
parser.add_argument('--scale', metavar='factor', type=float,
                    dest='scale', default=1.0,
                    help='designate an output scaling factor (default: 1.0)')
"""

args = parser.parse_args()
# args.color = args.color == 'true'
# convert(args.smiles_filename, output_path=args.output_path, output_format=args.format, scale=args.scale, color=args.color)
convert(args.smiles_filename, output_path=args.output_path)
exit(0)
