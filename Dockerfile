FROM daverona/rdkit:Release_2019_09_3

COPY smi2img.py /usr/local/bin/smi2img
RUN chmod a+x /usr/local/bin/smi2img \
  && apk add --no-cache ttf-opensans \
  && fc-cache -f && rm -rf /var/cache/*

WORKDIR /var/local

CMD ["/usr/local/bin/smi2img", "--help"]
